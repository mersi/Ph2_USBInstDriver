/*

    \file                          HMP4040Controller.h
    \brief                         Server class to communicate with HMP404 instrument via usbtmc and SCPI - this is the server class
    \author                        Georg Auzinger
    \version                       1.0
    \date                          22/09/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef _HMP4040SERVER__
#define _HMP4040SERVER__
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <vector>
#include <utility>
#include <regex>
#include <thread>
#include <mutex>
#include "../Drivers/UsbTmcHandler.h"
#include "../Drivers/SerialHandler.h"
#include "../Utils/easylogging++.h"
#include "../Utils/ConfigParser.cc"
#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"
#include "THttpServer.h"
#include "TH1F.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TROOT.h"

namespace Ph2_UsbInst {

    class HMP4040Controller
    {
      private:
        // usbtmc handler
        SerialHandler* fHandler;
        THttpServer* fServer;
        TMultiGraph* fGraph;
        std::vector<TGraph*> fGraphVec;

      public:
        // "Data"
        MeasurementValues fValues;
        std::vector<std::pair<double, double>> fConfigVec;

      private:
        // for log file IO
        std::string fFileName;
        std::ofstream fFile;
        bool fFileOpen;

        // for threaded workloop during monitoring
        std::thread fThread;
        std::mutex fInterfaceMutex;
        std::mutex fMemberMutex;
        std::atomic<int> fReadInterval;
        std::atomic<bool> fMonitoringRun;
        std::atomic<bool> fMonitoringPaused;


      public:
        HMP4040Controller (THttpServer* pServer);
        HMP4040Controller();
        ~HMP4040Controller();

        //  make this private once the server client architecture and the worker is implemented
        void InitConfigFile (std::string pFilename);
        void Configure();
        void Reset();

        //per channel Basis
        void SelectChannel (uint32_t pChannel);
        int GetChannelAcitve();
        void ConfigChannel (uint32_t pChannel, double pVoltage, double pCurrent);
        void ConfigVoltage (uint32_t pChannel, double pVoltage);
        void ConfigCurrent (uint32_t pChannel, double pCurrent);
        void SetOutputState (uint32_t pChannel, bool pState);
        bool GetOutputState (uint32_t pChannel);
        void MeasureVoltage (uint32_t pChannel);
        void MeasureCurrent (uint32_t pChannel);
        MeasurementValues GetLatestReadValues();

        //global basis
        void ApplyConfig(); // to be used when Voltage and Current Vector are filled from config file and in Configure method
        void MeasureAll();
        void ToggleGlobalOutput (bool pState);

        //monitoring functions / workloop
        void SetLogFileName (std::string pFilename);
        void StartMonitoring (uint32_t pReadInterval = 2);
        void StopMonitoring();
        void PauseMonitoring()
        {
            fMonitoringPaused = true;
            fFile.flush();
        }
        void ResumeMonitoring()
        {
            fMonitoringPaused = false;
            //fFile.flush();
        }
        void MonitoringWorkloop ();
        void PrintValues (std::ofstream& os);

        //system functions
        void SystemBeeper();
        std::string SystemError();
        void SystemLocal();
        void SystemRemote();
        void SystemMix();
        void SystemGetInfo();

      private:
        // to stream "Data" to file easily
        // log file handling
        void openLogFile ();
        void closeLogFile();
        void getTimestamp();
        void initGraphs();
        void clearGraphs();
        void fillGraphs();
        void prettifyGraph (TGraph* pGraph);
        // facilitates SCPI message creation
        const std::string lf = "\n";
        std::string formatString (std::string pString)
        {
            return pString + lf;
        }
        const std::string formatDateTime()
        {
            struct tm tstruct;
            char buf[80];
            tstruct = *localtime ( &fValues.fTimestamp );
            // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
            // for more information about date/time format
            strftime ( buf, sizeof ( buf ), "%d-%m-%y %H:%M", &tstruct );
            return buf;
        }
    };
}

#endif
