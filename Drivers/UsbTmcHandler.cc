#include "UsbTmcHandler.h"

using namespace Ph2_UsbInst;


UsbTmcHandler::UsbTmcHandler (std::string pDevName) : fMinorId (1)
{
    this->openFile (fMinorId, pDevName);
}


// Destructor needs to close fFile!
UsbTmcHandler::~UsbTmcHandler()
{
    if (fFileOpen) close (fFile);
}

// generic write method that accepts SCPI command as string!
std::string UsbTmcHandler::devwrite (const std::string pMsg)
{
    errno = 0;
    int retval = 0;

    if (fFileOpen)
    {
        if (!pMsg.empty() )
            retval = write (fFile, pMsg.c_str(), pMsg.size() );
        else
        {
            LOG (ERROR) << RED << "Message to send is empty!" << RESET;
            return "";
        }

        if (retval == -1)
        {
            perror ("UsbTmcHandler::devwrite - write: ");
            errno = 0;
            return "";
        }

        //read back
        char buffer[buffsize];
        int actual = read (fFile, buffer, buffsize);

        if (actual == -1)
        {
            perror ("UsbTmcHandler::devwrite - read: ");
            errno = 0;
            return "";
        }
        else
        {
            buffer[actual] = 0;
            std::string cReply = std::string (buffer);
            //LOG (DEBUG) << "Read: " << cReply ;
            return cReply;
        }
    }
    else
    {
        LOG (ERROR) << RED << "Error: the device file is not open!" << RESET ;
        return "";
    }
}

// generic read method that accepts SCPI command as string!
std::string UsbTmcHandler::devread ()
{
    errno = 0;

    if (fFileOpen)
    {
        //read response
        char buffer[buffsize];
        int actual = read (fFile, buffer, buffsize);

        if (actual == -1)
        {
            perror ("UsbTmcHandler::devread: ");
            errno = 0;
            return "";
        }
        else
        {
            buffer[actual] = 0;
            std::string cReply (buffer);
            //LOG (DEBUG) << "Read (devread): " << cReply;
            return cReply;
        }
    }
    else return "";
}

void UsbTmcHandler::simplewrite (const std::string pMsg)
{
    errno = 0;
    int retval = 0;

    if (fFileOpen)
    {
        if (!pMsg.empty() )
            retval = write (fFile, pMsg.c_str(), pMsg.size() );
        else
            LOG (ERROR) << RED << "Message to send is empty!" << RESET;

        if (retval == -1)
        {
            perror ("UsbTmcHandler::simplewrite: ");
            errno = 0;
        }
    }

    else
        LOG (ERROR) << RED << "Error: the device file is not open!" << RESET;
}

void UsbTmcHandler::openFile ( int pMinorId, std::string pDevName)
{
    bool cCorrect = false;
    //open the corresponding device file and set mode to read
    std::string cFilename = "/dev/usbtmc" + std::to_string (pMinorId);

    fFile = open (cFilename.c_str(), O_RDWR  | O_NONBLOCK);

    if (fFile < 0)
        LOG (ERROR) << RED << "Can not open device file " << cFilename << RESET;
    else
    {
        // we want to use read() not fread()
        fAttr.attribute = USBTMC_ATTRIB_READ_MODE;
        fAttr.value = USBTMC_ATTRIB_VAL_READ;
        ioctl (fFile, USBTMC_IOCTL_SET_ATTRIBUTE, &fAttr);

        //change the timeout to 2s
        fAttr.attribute = USBTMC_ATTRIB_TIMEOUT;
        fAttr.value = 2000;
        ioctl (fFile, USBTMC_IOCTL_SET_ATTRIBUTE, &fAttr);

        // we tell the driver that I want to auto abort on ERROR
        fAttr.attribute = USBTMC_ATTRIB_AUTO_ABORT_ON_ERROR;
        fAttr.value = USBTMC_ATTRIB_VAL_ON;
        ioctl (fFile, USBTMC_IOCTL_SET_ATTRIBUTE, &fAttr);

        fAttr.attribute = USBTMC_ATTRIB_NUM_INSTRUMENTS;
        ioctl (fFile, USBTMC_IOCTL_SET_ATTRIBUTE, &fAttr);

        fNInst = fAttr.value;
        LOG (INFO) << "Found " << fNInst << " usbtmc instruments connected!";

        for (uint32_t cMinor = pMinorId; cMinor <= fNInst; cMinor++)
        {
            //start trying with 1
            fInst.minor_number = cMinor;

            //check if minor ID 1 is already the correct one?
            if (ioctl (fFile, USBTMC_IOCTL_INSTRUMENT_DATA, &fInst) != -1)
                LOG (INFO) << BLUE << "Detected " << fInst.manufacturer << " " << fInst.product << " with minor ID " << fMinorId << RESET;

            std::string cModelId = static_cast<std::string> (fInst.manufacturer) + " " + static_cast<std::string> (fInst.product);
            std::regex e (pDevName);

            //yes? -> already have the good file open, nothing to do!
            if (std::regex_match (cModelId, e) )
            {
                fMinorId = cMinor;
                cCorrect = true;
                LOG (INFO) << BOLDGREEN << "Successfully connected to the correct Instrument \"" << cModelId << "\" with minor ID " << cMinor << RESET;
                break;
            }
            //no? -> need to close file and open the one with the correct id
            else
            {
                LOG (ERROR) << RED << "The current usbtmc device number (MINOR) " << cMinor << " is not the correct instrument, trying next minor!" << RESET;
                cCorrect = false;
                break;
            }
        }
    }

    if (!cCorrect)
    {
        close (fFile);
        this->openFile (pMinorId++, pDevName);
    }
    else fFileOpen = true;
}
