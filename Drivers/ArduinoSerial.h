//shamelessly copied from: https://github.com/todbot/arduino-serial
#ifndef __ARDUINOSERIAL_H__
#define __ARDUINOSERIAL_H__

#include <stdint.h>   // Standard types 
#include <stdio.h>    // Standard input/output definitions 
#include <unistd.h>   // UNIX standard function definitions 
#include <fcntl.h>    // File control definitions 
#include <errno.h>    // Error number definitions 
#include <termios.h>  // POSIX terminal control definitions 
#include <string.h>   // String function definitions 
#include <sys/ioctl.h>
#include "../Utils/easylogging++.h"

#define BUFMAX 128
//#define SERIALPORTDEBUG
class ArduinoSerial
{
  public:
    ArduinoSerial (std::string pDevName, int baud = 9600)
    {
        fDevName = pDevName;
        fFileDescriptor = serialport_init (pDevName.c_str(), baud);

        if (fFileDescriptor == -1)
            LOG (ERROR) << "Error, could not open serial port:" << pDevName;
        else
        {
            LOG (INFO) << "Successfully opened serial port: " << pDevName;
            serialport_flush (fFileDescriptor);
        }
    }
    ~ArduinoSerial()
    {
        if (fFileDescriptor == -1)
            LOG (ERROR) << "Error, closing serial port:" << fDevName;
        else
            serialport_close (fFileDescriptor);
    }

    int devwrite (std::string pMsg)
    {
        if (fFileDescriptor == -1)
        {
            LOG (ERROR) << "Error, could not read from serial port:" << fDevName;
            return -1;
        }
        else
        {
            pMsg += "\n";
            int rc = serialport_write (fFileDescriptor, pMsg.c_str() );

            if (rc == -1)
                LOG (ERROR) << "Error, could not write to serial port: " << fDevName;

            return rc;
        }

    }
    std::string devread()
    {
        if (fFileDescriptor == -1)
        {
            LOG (ERROR) << "Error, could not read from serial port:" << fDevName;
            return "";
        }
        else
        {
            char cBuf[BUFMAX];
            memset (cBuf, 0, BUFMAX);
            serialport_read_until (fFileDescriptor, cBuf, eolchar, BUFMAX, timeout);
            std::string str (cBuf, std::find (cBuf, cBuf + BUFMAX, '\0') );
            return str;
        }
    }

  private:
    // takes the string name of the serial port (e.g. "/dev/tty.usbserial","COM1")
    // and a baud rate (bps) and connects to that port at that speed and 8N1.
    // opens the port in fully raw mode so you can send binary data.
    // returns valid fd, or -1 on error
    int serialport_init (const char* serialport, int baud)
    {
        struct termios toptions;
        int fd;

        //fd = open(serialport, O_RDWR | O_NOCTTY | O_NDELAY);
        fd = open (serialport, O_RDWR | O_NONBLOCK );

        if (fd == -1)
        {
            perror ("serialport_init: Unable to open port ");
            return -1;
        }

        //int iflags = TIOCM_DTR;
        //ioctl(fd, TIOCMBIS, &iflags);     // turn on DTR
        //ioctl(fd, TIOCMBIC, &iflags);    // turn off DTR

        if (tcgetattr (fd, &toptions) < 0)
        {
            perror ("serialport_init: Couldn't get term attributes");
            return -1;
        }

        speed_t brate = baud; // let you override switch below if needed

        switch (baud)
        {
            case 4800:
                brate = B4800;
                break;

            case 9600:
                brate = B9600;
                break;
#ifdef B14400

            case 14400:
                brate = B14400;
                break;
#endif

            case 19200:
                brate = B19200;
                break;
#ifdef B28800

            case 28800:
                brate = B28800;
                break;
#endif

            case 38400:
                brate = B38400;
                break;

            case 57600:
                brate = B57600;
                break;

            case 115200:
                brate = B115200;
                break;
        }

        cfsetispeed (&toptions, brate);
        cfsetospeed (&toptions, brate);

        // 8N1
        toptions.c_cflag &= ~PARENB;
        toptions.c_cflag &= ~CSTOPB;
        toptions.c_cflag &= ~CSIZE;
        toptions.c_cflag |= CS8;
        // no flow control
        toptions.c_cflag &= ~CRTSCTS;

        //toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

        toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
        toptions.c_iflag &= ~ (IXON | IXOFF | IXANY); // turn off s/w flow ctrl

        toptions.c_lflag &= ~ (ICANON | ECHO | ECHOE | ISIG); // make raw
        toptions.c_oflag &= ~OPOST; // make raw

        // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
        toptions.c_cc[VMIN]  = 0;
        toptions.c_cc[VTIME] = 0;
        //toptions.c_cc[VTIME] = 20;

        tcsetattr (fd, TCSANOW, &toptions);

        if ( tcsetattr (fd, TCSAFLUSH, &toptions) < 0)
        {
            perror ("init_serialport: Couldn't set term attributes");
            return -1;
        }

        return fd;
    }

    //
    int serialport_close ( int fd )
    {
        return close ( fd );
    }

    //
    int serialport_writebyte ( int fd, uint8_t b)
    {
        int n = write (fd, &b, 1);

        if ( n != 1)
            return -1;

        return 0;
    }

    //
    int serialport_write (int fd, const char* str)
    {
        int len = strlen (str);
        int n = write (fd, str, len);

        if ( n != len )
        {
            perror ("serialport_write: couldn't write whole string\n");
            return -1;
        }

        return 0;
    }

    //
    int serialport_read_until (int fd, char* buf, char until, int buf_max, int timeout)
    {
        char b[1];  // read expects an array, so we give it a 1-byte array
        int i = 0;

        do
        {
            int n = read (fd, b, 1); // read a char at a time

            if ( n == -1) return -1; // couldn't read

            if ( n == 0 )
            {
                usleep ( 1 * 1000 ); // wait 1 msec try again
                timeout--;

                if ( timeout == 0 ) return -2;

                continue;
            }

#ifdef SERIALPORTDEBUG
            printf ("serialport_read_until: i=%d, n=%d b='%c'\n", i, n, b[0]); // debug
#endif
            buf[i] = b[0];
            i++;
        }
        while ( b[0] != until && i < buf_max && timeout > 0 );

        buf[i] = 0;  // null terminate the string
        return 0;
    }

    //
    int serialport_flush (int fd)
    {
        sleep (2); //required to make flush work, for some reason
        return tcflush (fd, TCIOFLUSH);
    }

    int fFileDescriptor;
    std::string fDevName;
    static const char eolchar = '\n';
    static const int timeout = 5000;
};
#endif
