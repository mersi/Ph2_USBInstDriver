/*

    \file                          ArdNano.h
    \brief                         Server class to communicate with Arduino Nano ATmega328 board via serial port - based on G.Auzinger's HMP4040Controller
    \author                        Sarah Seif El Nasr-Storey
    \version                       1.0
    \date                          13/01/2017
    Support :                      mail to : sarah.storey@SPAMNOT.cern.ch

 */

#ifndef _ARDNANO_
#define _ARDNANO_
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <vector>
#include <utility>
#include <regex>
#include "../Drivers/ArduinoSerial.h"
#include "../Drivers/SerialHandler.h"
#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"

namespace Ph2_UsbInst {

    class ArdNanoController
    {
      private:
        ArduinoSerial* fHandler;
        std::string fReply;

      public:
        ArdNanoController ();
        ~ArdNanoController();
        bool CheckArduinoState();

        //
        std::string getLastReply()
        {
            return fReply;
        };

        //  make this private once the server client architecture and the worker is implemented
        uint8_t Write (std::string pMessage);
        std::string Read();

        // functions to controlw the state of the on-board LED/Relay
        void ControlLED (uint8_t pLedState);
        void ControlRelay (uint8_t pRelayState);
        uint8_t GetRelayState();

      private:
        // facilitates SCPI message creation
        const std::string lf = "\n";
        std::string formatString (std::string pString)
        {
            return pString + lf;
        }
    };
}

#endif
