#ifndef _MESSAGES_H__
#define _MESSAGES_H__


#define STARTMONITOR "Starting monitoring"
#define STOPMONITOR "Stopping monitoring"
#define RESETINST "Resetting instrument"
#define CONFIGURE "Configured Instrument"
#define QUIT "Quitting application"
#define OUTPUT "Switching output"
#define CONFIGFILE "Initializing Config file: "
#define LOGFILE "Setting log filename to: "
#define VALUES "Values"
#define PAUSE "Pausing Monitoring"
#define RESUME "Resuming Monitoring"

#endif
